package de.maggicraft.mautomate;

public final class MConditions {

	private MConditions() { }

	/**
	 * Bedingung mit TimeOut in Millisekunden.
	 */
	public static MCondition condTimeOut(int pMillis) {
		return new MCondition() {

			private long mMaxTime;

			@Override
			public void init() {
				mMaxTime = System.currentTimeMillis() + pMillis;
			}

			@Override
			public boolean iterate() {
				return System.currentTimeMillis() < mMaxTime;
			}
		};
	}

	public static MCondition condTries(int pMillis) {
		return new MCondition() {

			private long mTries = 0;

			@Override
			public boolean iterate() {
				mTries++;
				return mTries < pMillis;
			}
		};
	}

}
