package de.maggicraft.mautomate;

import java.awt.event.KeyEvent;
import java.util.HashMap;

public final class MKeys {

	private static final HashMap<Character, IKey> KEY_MAP = new HashMap<>(100);

	private MKeys() { }

	static {
		for (int i = (int) '0'; i <= (int) '9'; i++) {
			key((char) i, i);
		}
		final int offset = 'A' - 'a';
		for (int i = (int) 'a'; i <= (int) 'z'; i++) {
			KEY_MAP.put((char) i, new MKey(i + offset));
		}
		for (int i = (int) 'A'; i <= (int) 'Z'; i++) {
			KEY_MAP.put((char) i, new MKeyShift(i));
		}
		shift('!', KeyEvent.VK_1);
		shift('"', KeyEvent.VK_2);
		shift('§', KeyEvent.VK_3);
		shift('$', KeyEvent.VK_4);
		shift('%', KeyEvent.VK_5);
		shift('&', KeyEvent.VK_6);
		shift('/', KeyEvent.VK_7);
		shift('(', KeyEvent.VK_8);
		shift(')', KeyEvent.VK_9);
		shift('=', KeyEvent.VK_0);

		altGr('{', KeyEvent.VK_7);
		altGr('[', KeyEvent.VK_8);
		altGr(']', KeyEvent.VK_9);
		altGr('}', KeyEvent.VK_0);

		key(',', KeyEvent.VK_COMMA);
		key('.', KeyEvent.VK_PERIOD);
		key(' ', KeyEvent.VK_SPACE);

		shift(';', KeyEvent.VK_COMMA);
		shift(':', KeyEvent.VK_PERIOD);

		key('<', KeyEvent.VK_LESS);
		shift('>', KeyEvent.VK_LESS);
		altGr('|', KeyEvent.VK_LESS);
		
		key('#', KeyEvent.VK_NUMBER_SIGN);
		shift('\'', KeyEvent.VK_NUMBER_SIGN);

		altGr('€', KeyEvent.VK_E);
		altGr('@', KeyEvent.VK_Q);

		key('+', KeyEvent.VK_PLUS);
		shift('*', KeyEvent.VK_PLUS);
		altGr('~', KeyEvent.VK_PLUS);

		key('-', KeyEvent.VK_MINUS);

		key('\n', KeyEvent.VK_ENTER);
		key('\t', KeyEvent.VK_TAB);
	}

	private static void key(char pChar, int pKeyCode) {
		KEY_MAP.put(pChar, new MKey(pKeyCode));
	}

	private static void shift(char pChar, int pKeyCode) {
		KEY_MAP.put(pChar, new MKeyShift(pKeyCode));
	}

	private static void altGr(char pChar, int pKeyCode) {
		KEY_MAP.put(pChar, new MKeyAltGr(pKeyCode));
	}

	public static void type(String pText) {
		for (int i = 0; i < pText.length(); i++) {
			type(pText.charAt(i));
			BotUtil.sleep(15);
		}
		BotUtil.sleep(45);
	}

	public static void type(char pKey) {
		IKey key = KEY_MAP.get(pKey);
		if (key == null) {
			System.err.println("'" + pKey + "': no such key in mappings");
		} else {
			key.type();
		}
	}

	public static IKey get(char pChar) {
		return KEY_MAP.get(pChar);
	}

}
