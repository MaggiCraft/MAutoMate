package de.maggicraft.mautomate;

public class MTuple<A, B> {

	private A mA;
	private B mB;

	public MTuple(A pA, B pB) {
		mA = pA;
		mB = pB;
	}

	@Override
	public String toString() {
		return mA + ", " + mB;
	}

	@Override
	public boolean equals(Object pO) {
		if (this == pO) { return true; }
		if (pO == null || getClass() != pO.getClass()) { return false; }
		MTuple<?, ?> tuple = (MTuple<?, ?>) pO;
		return mA.equals(tuple.mA) && mB.equals(tuple.mB);
	}

	@Override
	public int hashCode() {
		return mA.hashCode() + mB.hashCode();
	}

	public A getA() {
		return mA;
	}

	public B getB() {
		return mB;
	}
}
