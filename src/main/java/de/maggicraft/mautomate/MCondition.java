package de.maggicraft.mautomate;

/**
 * Created by Marc Schmidt on 27.05.2017.
 */
public abstract class MCondition {

	public void init() {

	}

	/**
	 * @return {@code true} if {@code iterate} should executed again, otherwise {@code false}
	 */
	public abstract boolean iterate();

}
