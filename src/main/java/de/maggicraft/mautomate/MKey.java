package de.maggicraft.mautomate;

import java.awt.*;

public class MKey implements IKey {

	protected static Robot sBot;

	protected int mKeyCode;

	public MKey(int pKeyCode) {
		mKeyCode = pKeyCode;
	}

	@Override
	public void type() {
		sBot.keyPress(mKeyCode);
		sBot.keyRelease(mKeyCode);
	}

	public static void setBot(Robot pBot) {
		sBot = pBot;
	}
}
