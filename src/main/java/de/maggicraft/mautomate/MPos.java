package de.maggicraft.mautomate;

/**
 * Gibt eine 2-dimensionale Position an.
 * <p>
 * Created by marc on 10.02.17.
 */
public final class MPos {

	private final int mPosX;
	private final int mPosY;

	public MPos(int mPosX, int mPosY) {
		this.mPosX = mPosX;
		this.mPosY = mPosY;
	}

	@Override
	public String toString() {
		return "MPos{ " + "posX: " + mPosX + ", posY: " + mPosY + '}';
	}

	public int getX() {
		return mPosX;
	}

	public int getY() {
		return mPosY;
	}
}
