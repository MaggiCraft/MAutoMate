package de.maggicraft.mautomate;

import java.awt.event.KeyEvent;

public class MKeyShift extends MKey {

  public MKeyShift(int pKeyCode) {
    super(pKeyCode);
  }

  @Override
  public void type() {
    try {
      sBot.keyPress(KeyEvent.VK_SHIFT);
      Thread.sleep(50);
      super.type();
      sBot.keyRelease(KeyEvent.VK_SHIFT);
      Thread.sleep(10);
    }
    catch (Exception pE) {
      pE.printStackTrace();
    }
  }
}
