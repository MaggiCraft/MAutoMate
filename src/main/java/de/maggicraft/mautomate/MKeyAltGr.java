package de.maggicraft.mautomate;

import java.awt.event.KeyEvent;

public class MKeyAltGr extends MKey {

	public MKeyAltGr(int pKeyCode) {
		super(pKeyCode);
	}

	@Override
	public void type() {
		try {
			sBot.keyPress(KeyEvent.VK_ALT);
			sBot.keyPress(KeyEvent.VK_CONTROL);
			Thread.sleep(50);
			super.type();
			sBot.keyRelease(KeyEvent.VK_ALT);
			sBot.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(50);
		} catch (Exception pE) {
			pE.printStackTrace();
		}
	}
}
