package de.maggicraft.mautomate;

import java.awt.event.InputEvent;

public final class MActions {

	private MActions() { }

	/**
	 * Gibt eine Aktion zurück, die die Maus zur übergebenen Position bewegt.
	 *
	 * @param pPos Position
	 */
	public static MAction actionMove(MPos pPos) {
		return new MAction(() -> {
			BotUtil.move(pPos);
			return true;
		}).addName("move to: " + pPos.getX() + ", " + pPos.getY());
	}

	/**
	 * Gibt eine Aktion zurück, die die Maus zur übergebenen Position bewegt.
	 *
	 * @param pPosX X-Koordinate
	 * @param pPosY Y-Koordinate
	 */
	public static MAction actionMove(int pPosX, int pPosY) {
		return new MAction(() -> {
			BotUtil.move(pPosX, pPosY);
			return true;
		}).addName("move to: " + pPosX + ", " + pPosY);
	}

	public static MAction actionMove(String... pPixels) {
		MPixels[] pixels = MPixels.make(pPixels);
		return new MAction(() -> BotUtil.move(pixels)).addName(MPixels.getName(pixels));
	}

	/**
	 * Gibt eine Aktion zurück, die an die übergebene Position klickt.
	 *
	 * @param pPos Position
	 */
	public static MAction actionClick(MPos pPos) {
		return new MAction(() -> {
			BotUtil.click(pPos);
			return true;
		}).addName("click at: " + pPos.getX() + ", " + pPos.getY());
	}

	/**
	 * Gibt eine Aktion zurück, die an die übergebene Position klickt.
	 *
	 * @param pPosX X-Koordinate
	 * @param pPosY Y-Koordinate
	 */
	public static MAction makeClick(int pPosX, int pPosY) {
		return new MAction(() -> {
			BotUtil.click(pPosX, pPosY);
			return true;
		}).addName("click at: " + pPosX + ", " + pPosY);
	}

	/**
	 * Gibt eine Aktion zurück, die an die übergebene Position mit dem übergebenen Maus-Button klickt.
	 *
	 * @param pPosX X-Koordinate
	 * @param pPosY Y-Koordinate
	 * @param pCode {@link InputEvent#BUTTON1_DOWN_MASK} linke Maustaste
	 */
	public static MAction actionClick(int pPosX, int pPosY, int pCode) {
		return new MAction(() -> {
			BotUtil.click(pPosX, pPosY, pCode);
			return true;
		}).addName("click at: " + pPosX + ", " + pPosY + ", with code: " + pCode);
	}

	public static MAction actionClick(String... pPixels) {
		MPixels[] pixels = MPixels.make(pPixels);
		return new MAction(() -> BotUtil.click(pixels)).addName(MPixels.getName(pixels));
	}

	public static MAction actionClick(int pCorner, String... pPixels) {
		MPixels[] pixels = MPixels.make(pPixels);
		return new MAction(() -> BotUtil.click(pixels, pCorner)).addName(MPixels.getName(pixels) + ", corner: " + pCorner);
	}

	public static MAction defClick(String... pPixels) {
		return defClick(125, 2500, pPixels);
	}

	public static MAction defClick(int pSleepAfter, int pTimeOut, String... pPixels) {
		return actionClick(pPixels).addAfterRun(() -> {
			BotUtil.move(0, 1080);
			BotUtil.sleep(pSleepAfter);
		}).addFailed(() -> System.exit(-1)).addCondition(MConditions.condTimeOut(pTimeOut));
	}

	public static MAction defMove(Runnable pRun, int pTimeOut, String... pPixels) {
		return actionMove(pPixels).addAfterRun(pRun).addFailed(() -> System.exit(-1)).addCondition(MConditions.condTimeOut(pTimeOut));
	}

	public static MAction actionWait(int pMillis, String... pPixels) {
		MPixels[] pixels = MPixels.make(pPixels);
		return new MAction(() -> MScreenshot.findPos(pixels) != null, MConditions.condTimeOut(pMillis)).addName("wait: " + MPixels.getName(pixels));
	}

}
