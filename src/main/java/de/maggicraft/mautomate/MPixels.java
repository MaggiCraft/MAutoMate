package de.maggicraft.mautomate;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Wrapper-Klasse für das {@link BufferedImage}.
 * <p>
 * Created by marc on 10.02.17.
 */
public class MPixels {

	/**
	 * Pfad aus dem die Bilder gelesen werden.
	 */
	private static String sPath;

	private byte[][][] mBytes;
	private boolean[][] mOpaque;
	private String mName;

	public static MPixels[] make(String... pNames) {
		HashSet<String> names = new HashSet<>();
		for (String name : pNames) {
			names.add(name);
			checkNameExtension(names, name);
		}

		MPixels[] pixels = new MPixels[names.size()];
		int i = 0;
		for (String name : names) {
			pixels[i] = new MPixels(sPath + name);
			i++;
		}
		return pixels;
	}

	private static void checkNameExtension(Set<String> pNames, String pName) {
		int len = pName.length();
		if (len > 2 && pName.charAt(len - 2) == '_') {
			char ch = pName.charAt(len - 1);
			ch++;
			String newName = pName.substring(0, len - 1) + ch;
			if (new File(sPath + pName.substring(0, len - 1) + ch + ".png").exists()) {
				pNames.add(newName);
				checkNameExtension(pNames, newName);
			}
		}
	}

	public static String getName(MPixels[] pPixels) {
		String names = "";
		for (MPixels pixel : pPixels) {
			if (pixel.mName != null) {
				names += pixel.mName + ", ";
			}
		}
		return names;
	}

	/**
	 * Erstellt ein {@code MPixels} mit übergebenen Pfad.
	 *
	 * @param pPath Pfad des Bildes, an diesen wird {@code .png} angehangen.
	 */
	public MPixels(String pPath) {
		File file = new File(pPath + ".png");
		try {
			initBytes(ImageIO.read(file), true);
		} catch (IOException pE) {
			new IOException(file.getAbsolutePath(), pE).printStackTrace();
		}
		mName = file.getName().replace(".png", "");
	}

	public MPixels(BufferedImage pImg) {
		this(pImg, false);
	}

	public MPixels(BufferedImage pImg, boolean pCheckTransparent) {
		initBytes(pImg, pCheckTransparent);
	}

	private void initBytes(BufferedImage pImg, boolean pCheckTransparent) {
		mBytes = new byte[3][pImg.getWidth()][pImg.getHeight()];

		if (pCheckTransparent) {
			mOpaque = new boolean[pImg.getWidth()][pImg.getHeight()];
			int pixel;
			for (int x = 0; x < mBytes[0].length; x++) {
				for (int y = 0; y < mBytes[0][0].length; y++) {
					pixel = pImg.getRGB(x, y);
					if ((pixel >> 24) != 0x00) {
						mOpaque[x][y] = true;
						mBytes[0][x][y] = (byte) (pixel & CBot.COLOR_SHIFT[0]);
						mBytes[1][x][y] = (byte) ((pixel & CBot.COLOR_SHIFT[1]) >> 8);
						mBytes[2][x][y] = (byte) ((pixel & CBot.COLOR_SHIFT[2]) >> 16);
					}
				}
			}
		} else {
			int pixel;
			for (int x = 0; x < mBytes[0].length; x++) {
				for (int y = 0; y < mBytes[0][0].length; y++) {
					pixel = pImg.getRGB(x, y);
					mBytes[0][x][y] = (byte) (pixel & CBot.COLOR_SHIFT[0]);
					mBytes[1][x][y] = (byte) ((pixel & CBot.COLOR_SHIFT[1]) >> 8);
					mBytes[2][x][y] = (byte) ((pixel & CBot.COLOR_SHIFT[2]) >> 16);
				}
			}
		}
	}

	public BufferedImage toImgAlpha() {
		BufferedImage img = new BufferedImage(mBytes[0].length, mBytes[0][0].length, BufferedImage.TYPE_INT_RGB);

		for (int x = 0; x < img.getWidth(); x++) {
			for (int y = 0; y < img.getHeight(); y++) {
				img.setRGB(x, y, new Color(mBytes[0][x][y] & 0xFF, mBytes[1][x][y] & 0xFF, mBytes[2][x][y] & 0xFF).getRGB());
			}
		}

		return img;
	}

	public BufferedImage toImg() {
		BufferedImage img = new BufferedImage(mBytes[0].length, mBytes[0][0].length, BufferedImage.TYPE_INT_RGB);

		for (int x = 0; x < img.getWidth(); x++) {
			for (int y = 0; y < img.getHeight(); y++) {
				img.setRGB(x, y, (0xFF << 24) | ((mBytes[2][x][y] & 0xFF) << 16) | ((mBytes[1][x][y] & 0xFF) << 8) | (mBytes[0][x][y] & 0xFF));
			}
		}

		return img;
	}

	public static void setPath(String pPath) {
		sPath = pPath;
	}

	public int getWidth() {
		return mBytes[0].length;
	}

	public int getHeight() {
		return mBytes[0][0].length;
	}

	public byte[][][] getBytes() {
		return mBytes;
	}

	public boolean[][] getOpaque() {
		return mOpaque;
	}

	public String getName() {
		return mName;
	}
}
