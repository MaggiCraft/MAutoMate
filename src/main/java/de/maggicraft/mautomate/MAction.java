package de.maggicraft.mautomate;

/**
 * Führt eine Funktion mit den übergebenen Parametern oder Bedingungen durch.
 * <p>
 * Created by Marc Schmidt on 27.05.2017.
 */
public class MAction {

	private static boolean sDisplay;

	private IAction mAction;
	private Runnable mFailed;
	/**
	 * Wird nach erfolgreicher Ausführung der {@link MAction#mAction} ausgeführt.
	 */
	private Runnable mAfterRun;
	private MCondition mCondition;
	private String mName;

	public MAction(IAction pAction) {
		mAction = pAction;
	}

	public MAction(IAction pAction, MCondition pCondition) {
		mAction = pAction;
		mCondition = pCondition;
	}

	public MAction addFailed(Runnable pFailed) {
		mFailed = pFailed;
		return this;
	}

	public MAction addAfterRun(Runnable pAfterRun) {
		mAfterRun = pAfterRun;
		return this;
	}

	public MAction addCondition(MCondition pCondition) {
		mCondition = pCondition;
		return this;
	}

	public MAction addName(String pName) {
		mName = pName;
		return this;
	}

	public boolean run() {
		boolean successful;
		if (mCondition == null) {
			successful = mAction.run();
		} else {
			mCondition.init();
			do {
				successful = mAction.run();
				if (sDisplay) {
					System.out.println(mName + ", iter: " + mCondition.iterate() + ", !successful: " + !successful);
				}
			} while (mCondition.iterate() && !successful);
		}

		if (successful) {
			if (mAfterRun != null) {
				mAfterRun.run();
			}
		} else {
			System.out.println("failed: " + mName);
			if (mFailed != null) {
				mFailed.run();
			}
		}
		return successful;
	}

	public static void setDisplay(boolean pDisplay) {
		sDisplay = pDisplay;
	}
}
