package de.maggicraft.mautomate;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * Created by Marc Schmidt on 27.05.2017.
 */
public class MScreenshot {

	/**
	 * Durchschnittliche Pixel-Abweichung die toleriert wird
	 */
	public static double sMaxAvrDif = 5;
	/**
	 * minimale Pixel-Abweichung die als Fehler erkannt wird
	 */
	public static int sMaxPixelDif = CBot.MAX_DIF;
	public static boolean sOutput = false;
	private static final Rectangle FULLSCREEN = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());

	/**
	 * Erstellt einen Screenshot des ganzen Bildschirms.
	 */
	public static MPixels takeShot() {
		return new MPixels(BotUtil.getBot().createScreenCapture(FULLSCREEN));
	}

	/**
	 * Erstellt einen Screenshot einer der vier Ecken des Bildschirms.
	 *
	 * @param pCorner Ecke und deren Viertel
	 */
	public static MPixels takeShot(int pCorner) {
		int halfWidth = FULLSCREEN.width / 2;
		int halfHeight = FULLSCREEN.height / 2;
		Rectangle rectangle;
		if (pCorner == CBot.UPPER_LEFT) {
			rectangle = new Rectangle(0, 0, halfWidth, halfHeight);
		} else if (pCorner == CBot.UPPER_RIGHT) {
			rectangle = new Rectangle(halfWidth, 0, halfWidth, halfHeight);
		} else if (pCorner == CBot.LOWER_RIGHT) {
			rectangle = new Rectangle(halfWidth, halfHeight, halfWidth, halfHeight);
		} else if (pCorner == CBot.LOWER_LEFT) {
			rectangle = new Rectangle(0, halfHeight, halfWidth, halfHeight);
		} else {
			throw new IllegalArgumentException();
		}
		return takeShot(rectangle);
	}

	/**
	 * Erstellt einen Screenshot mit entsprechenden Offset und Dimensionen.
	 * <p>
	 * Das Offset beginnt in der oberen linken Ecke des Bildschirms.
	 *
	 * @param pRect Offset und Dimension
	 */
	public static MPixels takeShot(Rectangle pRect) {
		return new MPixels(BotUtil.getBot().createScreenCapture(pRect), false);
	}

	public static MTuple<MPos, Integer> findPos(MPixels[] pObjects) {
		MPixels shot = takeShot();
		for (int i = 0; i < pObjects.length; i++) {
			MPos pos = findPos(pObjects[i], shot);
			if (pos != null) {
				return new MTuple<>(pos, i);
			}
		}
		return null;
	}

	public static MTuple<MPos, Integer> findPos(MPixels[] pObjects, int pCorner) {
		return findPos(pObjects, takeShot(pCorner));
	}

	public static MTuple<MPos, Integer> findPos(MPixels[] pObjects, MPixels pShot) {
		for (int i = 0; i < pObjects.length; i++) {
			MPos pos = findPos(pObjects[i], pShot);
			if (pos != null) {
				return new MTuple<>(pos, i);
			}
		}
		return null;
	}

	public static MPos findPos(MPixels pObject, MPixels pShot) {
		int screenWidth = pShot.getWidth() - pObject.getWidth();
		int screenHeight = pShot.getHeight() - pObject.getHeight();

		for (int x = 0; x < screenWidth; x++) {
			for (int y = 0; y < screenHeight; y++) {
				if (isSame(/*"", "",*/ x, y, pObject.getOpaque(), pObject.getBytes(), pShot.getBytes())) {
					if (sOutput) {
						String name = "shot_" + (new Random().nextInt(1000)) + ".png";
						System.out.println("name: " + name);
						try {
							ImageIO.write(pShot.toImg(), "PNG", new File("C:\\Users\\Marc Schmidt\\Downloads\\shots\\" + name));
						} catch (IOException pE) {
							pE.printStackTrace();
						}
					}
					return new MPos(x, y);
				}
			}
		}

		return null;
	}

	private static boolean isSame(/*String pObjectName, String pShotName, */int pPosX, int pPosY, boolean[][] pOpaque, byte[][][] pObject,
																			byte[][][] pShot) {
		int width = pObject[0].length;
		int height = pObject[0][0].length;

		int difQuan = 0;
		int dif = 0;
		int curDif;
		int visitedPixels = 0;

		for (int x = 0; x < width; x += 2) {
			for (int y = 0; y < height; y += 2) {
				if (pOpaque[x][y]) {
					for (int i = 0; i < 3; i++) {
						curDif = Math.abs((pObject[i][x][y] & 0xFF) - (pShot[i][pPosX + x][pPosY + y] & 0xFF));
						if (curDif >= sMaxPixelDif) {
							difQuan++;
							if ((double) difQuan / (double) visitedPixels > 0.075D) {
								return false;
							}
						}
						visitedPixels++;
						dif += curDif;
					}
				}
			}
		}

		double avrDif = dif / (double) visitedPixels;

		if (sOutput && avrDif < sMaxAvrDif) {
			System.out.println("x: " + pPosX + ", y: " + pPosY);
			System.out.println((/*pObjectName + " -> " + pShotName + "\t" + */avrDif + "\t" + ((double) difQuan / (double) visitedPixels)).replace
					("" + " .", ", " + "" + "" + ""));
		}

		return avrDif < sMaxAvrDif;
	}
}
