package de.maggicraft.mautomate;

import java.awt.*;
import java.awt.event.InputEvent;

/**
 * Created by Marc Schmidt on 25.05.2017.
 */
public final class BotUtil {

	private static Robot sBot;

	private BotUtil() {}

	static {
		try {
			sBot = new Robot();
		} catch (AWTException pE) {
			pE.printStackTrace();
		}

		MKey.setBot(sBot);
	}

	/**
	 * Bewegt die Maus zur übergebenen Position.
	 *
	 * @param pPos neue Position der Maus
	 */
	public static void move(MPos pPos) {
		sBot.mouseMove(pPos.getX(), pPos.getY());
	}

	/**
	 * Bewegt die Maus zur übergebenen Position.
	 *
	 * @param pPosX X-Koordinate
	 * @param pPosY Y-Koordinate
	 */
	public static void move(int pPosX, int pPosY) {
		sBot.mouseMove(pPosX, pPosY);
	}

	public static void moveSmoothTo(int pPosX, int pPosY) {
		Point point = MouseInfo.getPointerInfo().getLocation();
		moveSmooth(pPosX - point.x, pPosY - point.y);
	}

	public static void moveSmooth(int pOffsetX, int pOffsetY) {
		Point point = MouseInfo.getPointerInfo().getLocation();
		int steps = Math.max(Math.abs(pOffsetX), Math.abs(pOffsetY)) / 3;
		double stepX = pOffsetX / (double) steps;
		double stepY = pOffsetY / (double) steps;

		for (int i = 0; i < steps; i++) {
			sBot.mouseMove(point.x + (int) Math.round(stepX * i), point.y + (int) Math.round(stepY * i));
			sleep(10);
		}
	}

	public static boolean move(MPixels[] pObjects) {
		MTuple<MPos, Integer> tuple = MScreenshot.findPos(pObjects);
		if (tuple == null) {
			return false;
		} else {
			MPos pos = tuple.getA();
			MPixels object = pObjects[tuple.getB()];
			move(pos.getX() + object.getWidth() / 2, pos.getY() + object.getHeight() / 2);
			return true;
		}
	}

	public static boolean click(MPixels[] pObjects) {
		MTuple<MPos, Integer> tuple = MScreenshot.findPos(pObjects);
		if (tuple == null) {
			return false;
		} else {
			MPos pos = tuple.getA();
			MPixels object = pObjects[tuple.getB()];
			click(pos.getX() + object.getWidth() / 2, pos.getY() + object.getHeight() / 2);
			return true;
		}
	}

	public static boolean click(MPixels[] pObjects, int pCorner) {
		MTuple<MPos, Integer> tuple = MScreenshot.findPos(pObjects, pCorner);
		if (tuple == null) {
			return false;
		} else {
			MPos pos = tuple.getA();
			MPixels object = pObjects[tuple.getB()];
			click(pos.getX() + object.getWidth() / 2, pos.getY() + object.getHeight() / 2);
			return true;
		}
	}

	public static boolean click(MPixels[] pObjects, MPixels pShot) {
		MTuple<MPos, Integer> tuple = MScreenshot.findPos(pObjects, pShot);
		if (tuple == null) {
			return false;
		} else {
			MPos pos = tuple.getA();
			MPixels object = pObjects[tuple.getB()];
			click(pos.getX() + object.getWidth() / 2, pos.getY() + object.getHeight() / 2);
			return true;
		}
	}

	/**
	 * Klickt mit der linken Maustaste an die übergebene Position.
	 *
	 * @param pPos Position
	 */
	public static void click(MPos pPos) {
		click(pPos.getX(), pPos.getY());
	}

	/**
	 * Klickt mit der linken Maustaste an die übergebene Position.
	 *
	 * @param pPosX X-Koordinate
	 * @param pPosY Y-Koordinate
	 */
	public static void click(int pPosX, int pPosY) {
		sBot.mouseMove(pPosX, pPosY);
		sBot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
		sBot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
	}

	/**
	 * Klickt mit der übergebenen Maustaste an die übergebene Position.
	 *
	 * @param pPosX X-Koordinate
	 * @param pPosY Y-Koordinate
	 * @param pCode {@link InputEvent#BUTTON1_DOWN_MASK} linke Maustaste
	 *              {@link InputEvent#BUTTON3_DOWN_MASK} rechte Maustaste
	 */
	public static void click(int pPosX, int pPosY, int pCode) {
		sBot.mouseMove(pPosX, pPosY);
		sBot.mousePress(pCode);
		sBot.mouseRelease(pCode);
	}

	public static void clickLeft(){
		click(InputEvent.BUTTON1_DOWN_MASK);
	}

	public static void clickRight(){
		click(InputEvent.BUTTON3_DOWN_MASK);
	}

	public static void click(int pCode){
		sBot.mousePress(pCode);
		sBot.mouseRelease(pCode);
	}

	public static void sleep(long pSleep) {
		try {
			Thread.sleep(pSleep);
		} catch (InterruptedException pE) {
			pE.printStackTrace();
		}
	}

	public static Robot getBot() {
		return sBot;
	}
}
