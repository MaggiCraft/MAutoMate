package de.maggicraft.mautomate;

public interface IAction {

	boolean run();

}
