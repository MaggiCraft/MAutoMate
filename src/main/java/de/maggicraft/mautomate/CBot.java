package de.maggicraft.mautomate;

/**
 * Created by marc on 10.02.17.
 */
public final class CBot {

	public static final int UPPER_LEFT = 0;
	public static final int UPPER_RIGHT = 1;
	public static final int LOWER_RIGHT = 2;
	public static final int LOWER_LEFT = 3;

	public static final int TRANSPARENT = 0;
	public static final int MAX_DIF = 15;
	public static final int[] COLOR_SHIFT = {0x000000ff, 0x0000ff00, 0x00ff0000};

	private CBot() { }
}
