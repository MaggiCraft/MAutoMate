package de.maggicraft.mautomate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

final class TestMConditions {

  /**
   * Testet dass der TimeOut mindestens so lange hält.
   */
  @Test
  public void testTimeOut() {
    MCondition cond = MConditions.condTimeOut(2000);
    cond.init();
    long time = System.currentTimeMillis();

    while (cond.iterate()) {
      try {
        Thread.sleep(100);
      }
      catch (InterruptedException pE) {
        pE.printStackTrace();
      }

    }

    Assertions.assertTrue(System.currentTimeMillis() - time >= 2000);
  }

  @Test
  public void testTries() {
    int tries = 11;
    MCondition cond = MConditions.condTries(tries);
    int tried = 0;

    do {
      tried++;
    } while (cond.iterate());

    Assertions.assertEquals(tries, tried);
  }
}
