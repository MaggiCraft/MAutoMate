package de.maggicraft.mautomate;

import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

final class TestMScreenshot {

  /**
   * Erstellt von jedem Viertel des Bildschirms ein Aufnahme. Die Überprüfung erfolgt per Hand.
   */
  @Test
  void testScreenshotCorners() {
    int[] corners = { CBot.UPPER_LEFT, CBot.UPPER_RIGHT, CBot.LOWER_RIGHT, CBot.LOWER_LEFT };
    String[] name = { "UPPER_LEFT", "UPPER_RIGHT", "LOWER_RIGHT", "LOWER_LEFT" };
    for (int i = 0; i < corners.length; i++) {
      MPixels shot = MScreenshot.takeShot(corners[i]);
      try {
        Path path = Paths.get("shots/shot_" + name[i] + ".png").toAbsolutePath();
        Files.createDirectories(path);
        ImageIO.write(shot.toImg(), "PNG", path.toFile());
      }
      catch (IOException pE) {
        pE.printStackTrace();
      }
    }
  }

}
