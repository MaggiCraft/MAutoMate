package de.maggicraft.mautomate;

import de.maggicraft.mtest.TestUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

final class TestMPos {

  /**
   * Testet das korrekte Speichern einer Position.
   */
  @Test
  void testMPos() {
    int posX = TestUtil.rdm(1920) + 1;
    int posY = TestUtil.rdm(1080) + 1;
    MPos pos = new MPos(posX, posY);
    Assertions.assertEquals(posX, pos.getX());
    Assertions.assertEquals(posY, pos.getY());
  }

}
