package de.maggicraft.mautomate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import javax.swing.*;
import java.util.stream.Stream;

/**
 * Testet die {@link MKeys}-Klasse.
 */
final class TestMKeys {

  private static final Character[] CHARS = { ' ', '!', '#', '$', '%', '&', '\'', '"', '(', ')', '*', '+', ',', '-',
      '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '@', 'A', 'B', 'C', 'D',
      'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
      '[', ']', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
      'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~', '€' };
  private static final char[] NO_TEST = { '\n', '\t' };
  private static final char[] NOT_SUPPORTED = { '`', '?', '\\', '_', '^', 'ß' };

  private static Stream<Character> chars() {
    return Stream.of(CHARS);
  }

  /**
   * Testet alle drückbaren Tasten.
   * <p>
   * Der Test schlägt fehl, wenn zu einem Char keine Tasteninformationen oder die falschen vorliegen. Damit der Test
   * durchgeführt werden kann, darf die Maus nicht verwendet werden.
   */
  @ParameterizedTest
  @MethodSource("chars")
  void testType(Character pCharacter) {
    JFrame frame = new JFrame();
    frame.setBounds(0, 0, 200, 100);
    JTextField field = new JTextField();
    frame.add(field);
    frame.setVisible(true);
    BotUtil.sleep(500);

    char ch = pCharacter;

    IKey key = MKeys.get(ch);

    Assertions.assertNotNull(key, "not included: " + ch);

    BotUtil.sleep(150);
    key.type();
    BotUtil.sleep(150);

    String text = field.getText();
    boolean condition = text.length() == 1 && ch == text.charAt(0);

    Assertions.assertTrue(condition, () -> {
      String message = "not typed correctly: field length: " + text.length();
      if (!text.isEmpty()) {
        message += ", firstChar: " + text.charAt(0);
      }
      message += ", expected: '" + ch + '\'';
      return message;
    });

    field.setText("");

    frame.dispose();
  }
}
